import {ClassFeatures} from "./classFeatures.js"
 
// Namespace Configuration Values
export const mez5e = {};
 
// ASCII Artwork
mez5e.ASCII = `_______________________________
______      ______ _____ _____
|  _  \\___  |  _  \\  ___|  ___|
| | | ( _ ) | | | |___ \\| |__
| | | / _ \\/\\ | | |   \\ \\  __|
| |/ / (_>  < |/ //\\__/ / |___
|___/ \\___/\\/___/ \\____/\\____/
_______________________________`;
 
 
/**
 * The set of Ability Scores used within the system
 * @type {Object}
 */
mez5e.abilities = {
  "str": "mez5e.AbilityStr",
  "dex": "mez5e.AbilityDex",
  "con": "mez5e.AbilityCon",
  "int": "mez5e.AbilityInt",
  "wis": "mez5e.AbilityWis",
  "cha": "mez5e.AbilityCha"
};
 
mez5e.abilityAbbreviations = {
  "str": "mez5e.AbilityStrAbbr",
  "dex": "mez5e.AbilityDexAbbr",
  "con": "mez5e.AbilityConAbbr",
  "int": "mez5e.AbilityIntAbbr",
  "wis": "mez5e.AbilityWisAbbr",
  "cha": "mez5e.AbilityChaAbbr"
};
 
/* -------------------------------------------- */
 
/**
 * Character alignment options
 * @type {Object}
 */
mez5e.alignments = {
  'lg': "mez5e.AlignmentLG",
  'ng': "mez5e.AlignmentNG",
  'cg': "mez5e.AlignmentCG",
  'ln': "mez5e.AlignmentLN",
  'tn': "mez5e.AlignmentTN",
  'cn': "mez5e.AlignmentCN",
  'le': "mez5e.AlignmentLE",
  'ne': "mez5e.AlignmentNE",
  'ce': "mez5e.AlignmentCE"
};
 
/* -------------------------------------------- */
 
/**
 * An enumeration of item attunement types
 * @enum {number}
 */
mez5e.attunementTypes = {
  NONE: 0,
  REQUIRED: 1,
  ATTUNED: 2,
}
 
/**
 * An enumeration of item attunement states
 * @type {{"0": string, "1": string, "2": string}}
 */
mez5e.attunements = {
  0: "mez5e.AttunementNone",
  1: "mez5e.AttunementRequired",
  2: "mez5e.AttunementAttuned"
};
 
/* -------------------------------------------- */
 
 
mez5e.weaponProficiencies = {
  "sim": "mez5e.WeaponSimpleProficiency",
  "mar": "mez5e.WeaponMartialProficiency"
};
 
mez5e.toolProficiencies = {
  "art": "mez5e.ToolArtisans",
  "disg": "mez5e.ToolDisguiseKit",
  "forg": "mez5e.ToolForgeryKit",
  "game": "mez5e.ToolGamingSet",
  "herb": "mez5e.ToolHerbalismKit",
  "music": "mez5e.ToolMusicalInstrument",
  "navg": "mez5e.ToolNavigators",
  "pois": "mez5e.ToolPoisonersKit",
  "thief": "mez5e.ToolThieves",
  "vehicle": "mez5e.ToolVehicle"
};
 
 
/* -------------------------------------------- */
 
/**
 * This Object defines the various lengths of time which can occur
 * @type {Object}
 */
mez5e.timePeriods = {
  "inst": "mez5e.TimeInst",
  "turn": "mez5e.TimeTurn",
  "round": "mez5e.TimeRound",
  "minute": "mez5e.TimeMinute",
  "hour": "mez5e.TimeHour",
  "day": "mez5e.TimeDay",
  "month": "mez5e.TimeMonth",
  "year": "mez5e.TimeYear",
  "perm": "mez5e.TimePerm",
  "spec": "mez5e.Special"
};
 
 
/* -------------------------------------------- */
 
/**
 * This describes the ways that an ability can be activated
 * @type {Object}
 */
mez5e.abilityActivationTypes = {
  "none": "mez5e.None",
  "action": "mez5e.Action",
  "bonus": "mez5e.BonusAction",
  "reaction": "mez5e.Reaction",
  "minute": mez5e.timePeriods.minute,
  "hour": mez5e.timePeriods.hour,
  "day": mez5e.timePeriods.day,
  "special": mez5e.timePeriods.spec,
  "legendary": "mez5e.LegAct",
  "lair": "mez5e.LairAct",
  "crew": "mez5e.VehicleCrewAction"
};
 
/* -------------------------------------------- */
 
 
mez5e.abilityConsumptionTypes = {
  "ammo": "mez5e.ConsumeAmmunition",
  "attribute": "mez5e.ConsumeAttribute",
  "material": "mez5e.ConsumeMaterial",
  "charges": "mez5e.ConsumeCharges"
};
 
 
/* -------------------------------------------- */
 
// Creature Sizes
mez5e.actorSizes = {
  "tiny": "mez5e.SizeTiny",
  "sm": "mez5e.SizeSmall",
  "med": "mez5e.SizeMedium",
  "lg": "mez5e.SizeLarge",
  "huge": "mez5e.SizeHuge",
  "grg": "mez5e.SizeGargantuan"
};
 
mez5e.tokenSizes = {
  "tiny": 1,
  "sm": 1,
  "med": 1,
  "lg": 2,
  "huge": 3,
  "grg": 4
};
 
/* -------------------------------------------- */
 
/**
 * Classification types for item action types
 * @type {Object}
 */
mez5e.itemActionTypes = {
  "mwak": "mez5e.ActionMWAK",
  "rwak": "mez5e.ActionRWAK",
  "msak": "mez5e.ActionMSAK",
  "rsak": "mez5e.ActionRSAK",
  "save": "mez5e.ActionSave",
  "heal": "mez5e.ActionHeal",
  "abil": "mez5e.ActionAbil",
  "util": "mez5e.ActionUtil",
  "other": "mez5e.ActionOther"
};
 
/* -------------------------------------------- */
 
mez5e.itemCapacityTypes = {
  "items": "mez5e.ItemContainerCapacityItems",
  "weight": "mez5e.ItemContainerCapacityWeight"
};
 
/* -------------------------------------------- */
 
/**
 * Enumerate the lengths of time over which an item can have limited use ability
 * @type {Object}
 */
mez5e.limitedUsePeriods = {
  "sr": "mez5e.ShortRest",
  "lr": "mez5e.LongRest",
  "day": "mez5e.Day",
  "charges": "mez5e.Charges"
};
 
 
/* -------------------------------------------- */
 
/**
 * The set of equipment types for armor, clothing, and other objects which can ber worn by the character
 * @type {Object}
 */
mez5e.equipmentTypes = {
  "light": "mez5e.EquipmentLight",
  "medium": "mez5e.EquipmentMedium",
  "heavy": "mez5e.EquipmentHeavy",
  "bonus": "mez5e.EquipmentBonus",
  "natural": "mez5e.EquipmentNatural",
  "shield": "mez5e.EquipmentShield",
  "clothing": "mez5e.EquipmentClothing",
  "trinket": "mez5e.EquipmentTrinket",
  "vehicle": "mez5e.EquipmentVehicle"
};
 
 
/* -------------------------------------------- */
 
/**
 * The set of Armor Proficiencies which a character may have
 * @type {Object}
 */
mez5e.armorProficiencies = {
  "lgt": mez5e.equipmentTypes.light,
  "med": mez5e.equipmentTypes.medium,
  "hvy": mez5e.equipmentTypes.heavy,
  "shl": "mez5e.EquipmentShieldProficiency"
};
 
 
/* -------------------------------------------- */
 
/**
 * Enumerate the valid consumable types which are recognized by the system
 * @type {Object}
 */
mez5e.consumableTypes = {
  "ammo": "mez5e.ConsumableAmmunition",
  "potion": "mez5e.ConsumablePotion",
  "poison": "mez5e.ConsumablePoison",
  "food": "mez5e.ConsumableFood",
  "scroll": "mez5e.ConsumableScroll",
  "wand": "mez5e.ConsumableWand",
  "rod": "mez5e.ConsumableRod",
  "trinket": "mez5e.ConsumableTrinket"
};
 
/* -------------------------------------------- */
 
/**
 * The valid currency denominations supported by the 5e system
 * @type {Object}
 */
mez5e.currencies = {
  "pp": "mez5e.CurrencyPP",
  "gp": "mez5e.CurrencyGP",
  "ep": "mez5e.CurrencyEP",
  "sp": "mez5e.CurrencySP",
  "cp": "mez5e.CurrencyCP",
};
 
 
/**
 * Define the upwards-conversion rules for registered currency types
 * @type {{string, object}}
 */
mez5e.currencyConversion = {
  cp: {into: "sp", each: 10},
  sp: {into: "ep", each: 5 },
  ep: {into: "gp", each: 2 },
  gp: {into: "pp", each: 10}
};
 
/* -------------------------------------------- */
 
 
// Damage Types
mez5e.damageTypes = {
  "acid": "mez5e.DamageAcid",
  "bludgeoning": "mez5e.DamageBludgeoning",
  "cold": "mez5e.DamageCold",
  "fire": "mez5e.DamageFire",
  "force": "mez5e.DamageForce",
  "lightning": "mez5e.DamageLightning",
  "necrotic": "mez5e.DamageNecrotic",
  "piercing": "mez5e.DamagePiercing",
  "poison": "mez5e.DamagePoison",
  "psychic": "mez5e.DamagePsychic",
  "radiant": "mez5e.DamageRadiant",
  "slashing": "mez5e.DamageSlashing",
  "thunder": "mez5e.DamageThunder"
};
 
// Damage Resistance Types
mez5e.damageResistanceTypes = mergeObject(duplicate(mez5e.damageTypes), {
  "physical": "mez5e.DamagePhysical"
});
 
 
/* -------------------------------------------- */
 
/**
 * The valid units of measure for movement distances in the game system.
 * By default this uses the imperial units of feet and miles.
 * @type {Object<string,string>}
 */
mez5e.movementTypes = {
  "burrow": "mez5e.MovementBurrow",
  "climb": "mez5e.MovementClimb",
  "fly": "mez5e.MovementFly",
  "swim": "mez5e.MovementSwim",
  "walk": "mez5e.MovementWalk",
}
 
/**
 * The valid units of measure for movement distances in the game system.
 * By default this uses the imperial units of feet and miles.
 * @type {Object<string,string>}
 */
mez5e.movementUnits = {
  "ft": "mez5e.DistFt",
  "mi": "mez5e.DistMi"
}
 
/**
 * The valid units of measure for the range of an action or effect.
 * This object automatically includes the movement units from mez5e.movementUnits
 * @type {Object<string,string>}
 */
mez5e.distanceUnits = {
  "none": "mez5e.None",
  "self": "mez5e.DistSelf",
  "touch": "mez5e.DistTouch",
  "spec": "mez5e.Special",
  "any": "mez5e.DistAny"
};
for ( let [k, v] of Object.entries(mez5e.movementUnits) ) {
  mez5e.distanceUnits[k] = v;
}
 
/* -------------------------------------------- */
 
 
/**
 * Configure aspects of encumbrance calculation so that it could be configured by modules
 * @type {Object}
 */
mez5e.encumbrance = {
  currencyPerWeight: 50,
  strMultiplier: 15,
  vehicleWeightMultiplier: 2000 // 2000 lbs in a ton
};
 
/* -------------------------------------------- */
 
/**
 * This Object defines the types of single or area targets which can be applied
 * @type {Object}
 */
mez5e.targetTypes = {
  "none": "mez5e.None",
  "self": "mez5e.TargetSelf",
  "creature": "mez5e.TargetCreature",
  "ally": "mez5e.TargetAlly",
  "enemy": "mez5e.TargetEnemy",
  "object": "mez5e.TargetObject",
  "space": "mez5e.TargetSpace",
  "radius": "mez5e.TargetRadius",
  "sphere": "mez5e.TargetSphere",
  "cylinder": "mez5e.TargetCylinder",
  "cone": "mez5e.TargetCone",
  "square": "mez5e.TargetSquare",
  "cube": "mez5e.TargetCube",
  "line": "mez5e.TargetLine",
  "wall": "mez5e.TargetWall"
};
 
 
/* -------------------------------------------- */
 
 
/**
 * Map the subset of target types which produce a template area of effect
 * The keys are mez5e target types and the values are MeasuredTemplate shape types
 * @type {Object}
 */
mez5e.areaTargetTypes = {
  cone: "cone",
  cube: "rect",
  cylinder: "circle",
  line: "ray",
  radius: "circle",
  sphere: "circle",
  square: "rect",
  wall: "ray"
};
 
 
/* -------------------------------------------- */
 
// Healing Types
mez5e.healingTypes = {
  "healing": "mez5e.Healing",
  "temphp": "mez5e.HealingTemp"
};
 
 
/* -------------------------------------------- */
 
 
/**
 * Enumerate the denominations of hit dice which can apply to classes
 * @type {Array.<string>}
 */
mez5e.hitDieTypes = ["d6", "d8", "d10", "d12"];
 
 
/* -------------------------------------------- */
 
/**
 * The set of possible sensory perception types which an Actor may have
 * @type {object}
 */
mez5e.senses = {
  "blindsight": "mez5e.SenseBlindsight",
  "darkvision": "mez5e.SenseDarkvision",
  "tremorsense": "mez5e.SenseTremorsense",
  "truesight": "mez5e.SenseTruesight"
};
 
/* -------------------------------------------- */
 
/**
 * The set of skill which can be trained
 * @type {Object}
 */
mez5e.skills = {
  "acr": "mez5e.SkillAcr",
  "ani": "mez5e.SkillAni",
  "arc": "mez5e.SkillArc", 
  "dat": "mez5e.SkillDat",
  "dec": "mez5e.SkillDec",
  "his": "mez5e.SkillHis",
  "ins": "mez5e.SkillIns",
  "itm": "mez5e.SkillItm",
  "inv": "mez5e.SkillInv",
  "med": "mez5e.SkillMed",
  "nat": "mez5e.SkillNat",
  "pil": "mez5e.SkillPil",
  "prc": "mez5e.SkillPrc",
  "prf": "mez5e.SkillPrf",
  "per": "mez5e.SkillPer",
  "rel": "mez5e.SkillRel",
  "slt": "mez5e.SkillSlt",
  "ste": "mez5e.SkillSte",
  "sur": "mez5e.SkillSur"
  "tec": "mez5e.SkillTec",
};
 
 
/* -------------------------------------------- */
 
mez5e.spellPreparationModes = {
  "prepared": "mez5e.SpellPrepPrepared",
  "pact": "mez5e.PactMagic",
  "always": "mez5e.SpellPrepAlways",
  "atwill": "mez5e.SpellPrepAtWill",
  "innate": "mez5e.SpellPrepInnate"
};
 
mez5e.spellUpcastModes = ["always", "pact", "prepared"];
 
mez5e.spellProgression = {
  "none": "mez5e.SpellNone",
  "full": "mez5e.SpellProgFull",
  "half": "mez5e.SpellProgHalf",
  "third": "mez5e.SpellProgThird",
  "pact": "mez5e.SpellProgPact",
  "artificer": "mez5e.SpellProgArt"
};
 
/* -------------------------------------------- */
 
/**
 * The available choices for how spell damage scaling may be computed
 * @type {Object}
 */
mez5e.spellScalingModes = {
  "none": "mez5e.SpellNone",
  "cantrip": "mez5e.SpellCantrip",
  "level": "mez5e.SpellLevel"
};
 
/* -------------------------------------------- */
 
 
/**
 * Define the set of types which a weapon item can take
 * @type {Object}
 */
mez5e.weaponTypes = {
  "simpleM": "mez5e.WeaponSimpleM",
  "simpleR": "mez5e.WeaponSimpleR",
  "martialM": "mez5e.WeaponMartialM",
  "martialR": "mez5e.WeaponMartialR",
  "natural": "mez5e.WeaponNatural",
  "improv": "mez5e.WeaponImprov",
  "siege": "mez5e.WeaponSiege"
};
 
 
/* -------------------------------------------- */
 
/**
 * Define the set of weapon property flags which can exist on a weapon
 * @type {Object}
 */
mez5e.weaponProperties = {
  "ada": "mez5e.WeaponPropertiesAda",
  "amm": "mez5e.WeaponPropertiesAmm",
  "fin": "mez5e.WeaponPropertiesFin",
  "fir": "mez5e.WeaponPropertiesFir",
  "foc": "mez5e.WeaponPropertiesFoc",
  "hvy": "mez5e.WeaponPropertiesHvy",
  "lgt": "mez5e.WeaponPropertiesLgt",
  "lod": "mez5e.WeaponPropertiesLod",
  "mgc": "mez5e.WeaponPropertiesMgc",
  "rch": "mez5e.WeaponPropertiesRch",
  "rel": "mez5e.WeaponPropertiesRel",
  "ret": "mez5e.WeaponPropertiesRet",
  "sil": "mez5e.WeaponPropertiesSil",
  "spc": "mez5e.WeaponPropertiesSpc",
  "thr": "mez5e.WeaponPropertiesThr",
  "two": "mez5e.WeaponPropertiesTwo",
  "ver": "mez5e.WeaponPropertiesVer"
};
 
 
// Spell Components
mez5e.spellComponents = {
  "V": "mez5e.ComponentVerbal",
  "S": "mez5e.ComponentSomatic",
  "M": "mez5e.ComponentMaterial"
};
 
// Spell Schools
mez5e.spellSchools = {
  "abj": "mez5e.SchoolAbj",
  "con": "mez5e.SchoolCon",
  "div": "mez5e.SchoolDiv",
  "enc": "mez5e.SchoolEnc",
  "evo": "mez5e.SchoolEvo",
  "ill": "mez5e.SchoolIll",
  "nec": "mez5e.SchoolNec",
  "trs": "mez5e.SchoolTrs"
};
 
// Spell Levels
mez5e.spellLevels = {
  0: "mez5e.SpellLevel0",
  1: "mez5e.SpellLevel1",
  2: "mez5e.SpellLevel2",
  3: "mez5e.SpellLevel3",
  4: "mez5e.SpellLevel4",
  5: "mez5e.SpellLevel5",
  6: "mez5e.SpellLevel6",
  7: "mez5e.SpellLevel7",
  8: "mez5e.SpellLevel8",
  9: "mez5e.SpellLevel9"
};
 
// Spell Scroll Compendium UUIDs
mez5e.spellScrollIds = {
  0: 'Compendium.mez5e.items.rQ6sO7HDWzqMhSI3',
  1: 'Compendium.mez5e.items.9GSfMg0VOA2b4uFN',
  2: 'Compendium.mez5e.items.XdDp6CKh9qEvPTuS',
  3: 'Compendium.mez5e.items.hqVKZie7x9w3Kqds',
  4: 'Compendium.mez5e.items.DM7hzgL836ZyUFB1',
  5: 'Compendium.mez5e.items.wa1VF8TXHmkrrR35',
  6: 'Compendium.mez5e.items.tI3rWx4bxefNCexS',
  7: 'Compendium.mez5e.items.mtyw4NS1s7j2EJaD',
  8: 'Compendium.mez5e.items.aOrinPg7yuDZEuWr',
  9: 'Compendium.mez5e.items.O4YbkJkLlnsgUszZ'
};
 
/**
 * Define the standard slot progression by character level.
 * The entries of this array represent the spell slot progression for a full spell-caster.
 * @type {Array[]}
 */
mez5e.SPELL_SLOT_TABLE = [
  [2],
  [3],
  [4, 2],
  [4, 3],
  [4, 3, 2],
  [4, 3, 3],
  [4, 3, 3, 1],
  [4, 3, 3, 2],
  [4, 3, 3, 3, 1],
  [4, 3, 3, 3, 2],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 2, 1, 1]
];
 
/* -------------------------------------------- */
 
// Polymorph options.
mez5e.polymorphSettings = {
  keepPhysical: 'mez5e.PolymorphKeepPhysical',
  keepMental: 'mez5e.PolymorphKeepMental',
  keepSaves: 'mez5e.PolymorphKeepSaves',
  keepSkills: 'mez5e.PolymorphKeepSkills',
  mergeSaves: 'mez5e.PolymorphMergeSaves',
  mergeSkills: 'mez5e.PolymorphMergeSkills',
  keepClass: 'mez5e.PolymorphKeepClass',
  keepFeats: 'mez5e.PolymorphKeepFeats',
  keepSpells: 'mez5e.PolymorphKeepSpells',
  keepItems: 'mez5e.PolymorphKeepItems',
  keepBio: 'mez5e.PolymorphKeepBio',
  keepVision: 'mez5e.PolymorphKeepVision'
};
 
/* -------------------------------------------- */
 
/**
 * Skill, ability, and tool proficiency levels
 * Each level provides a proficiency multiplier
 * @type {Object}
 */
mez5e.proficiencyLevels = {
  0: "mez5e.NotProficient",
  1: "mez5e.Proficient",
  0.5: "mez5e.HalfProficient",
  2: "mez5e.Expertise"
};
 
/* -------------------------------------------- */
 
/**
 * The amount of cover provided by an object.
 * In cases where multiple pieces of cover are
 * in play, we take the highest value.
 */
mez5e.cover = {
  0: 'mez5e.None',
  .5: 'mez5e.CoverHalf',
  .75: 'mez5e.CoverThreeQuarters',
  1: 'mez5e.CoverTotal'
};
 
/* -------------------------------------------- */
 
 
// Condition Types
mez5e.conditionTypes = {
  "blinded": "mez5e.ConBlinded",
  "charmed": "mez5e.ConCharmed",
  "deafened": "mez5e.ConDeafened",
  "diseased": "mez5e.ConDiseased",
  "exhaustion": "mez5e.ConExhaustion",
  "frightened": "mez5e.ConFrightened",
  "grappled": "mez5e.ConGrappled",
  "incapacitated": "mez5e.ConIncapacitated",
  "invisible": "mez5e.ConInvisible",
  "paralyzed": "mez5e.ConParalyzed",
  "petrified": "mez5e.ConPetrified",
  "poisoned": "mez5e.ConPoisoned",
  "prone": "mez5e.ConProne",
  "restrained": "mez5e.ConRestrained",
  "stunned": "mez5e.ConStunned",
  "unconscious": "mez5e.ConUnconscious"
};
 
// Languages
mez5e.languages = {
  "common": "mez5e.LanguagesCommon",
  "aarakocra": "mez5e.LanguagesAarakocra",
  "abyssal": "mez5e.LanguagesAbyssal",
  "aquan": "mez5e.LanguagesAquan",
  "auran": "mez5e.LanguagesAuran",
  "celestial": "mez5e.LanguagesCelestial",
  "deep": "mez5e.LanguagesDeepSpeech",
  "draconic": "mez5e.LanguagesDraconic",
  "druidic": "mez5e.LanguagesDruidic",
  "dwarvish": "mez5e.LanguagesDwarvish",
  "elvish": "mez5e.LanguagesElvish",
  "giant": "mez5e.LanguagesGiant",
  "gith": "mez5e.LanguagesGith",
  "gnomish": "mez5e.LanguagesGnomish",
  "goblin": "mez5e.LanguagesGoblin",
  "gnoll": "mez5e.LanguagesGnoll",
  "halfling": "mez5e.LanguagesHalfling",
  "ignan": "mez5e.LanguagesIgnan",
  "infernal": "mez5e.LanguagesInfernal",
  "orc": "mez5e.LanguagesOrc",
  "primordial": "mez5e.LanguagesPrimordial",
  "sylvan": "mez5e.LanguagesSylvan",
  "terran": "mez5e.LanguagesTerran",
  "cant": "mez5e.LanguagesThievesCant",
  "undercommon": "mez5e.LanguagesUndercommon"
};
 
// Character Level XP Requirements
mez5e.CHARACTER_EXP_LEVELS =  [
  0, 300, 900, 2700, 6500, 14000, 23000, 34000, 48000, 64000, 85000, 100000,
  120000, 140000, 165000, 195000, 225000, 265000, 305000, 355000]
;
 
// Challenge Rating XP Levels
mez5e.CR_EXP_LEVELS = [
  10, 200, 450, 700, 1100, 1800, 2300, 2900, 3900, 5000, 5900, 7200, 8400, 10000, 11500, 13000, 15000, 18000,
  20000, 22000, 25000, 33000, 41000, 50000, 62000, 75000, 90000, 105000, 120000, 135000, 155000
];
 
// Character Features Per Class And Level
mez5e.classFeatures = ClassFeatures;
 
// Configure Optional Character Flags
mez5e.characterFlags = {
  "diamondSoul": {
    name: "mez5e.FlagsDiamondSoul",
    hint: "mez5e.FlagsDiamondSoulHint",
    section: "Feats",
    type: Boolean
  },
  "elvenAccuracy": {
    name: "mez5e.FlagsElvenAccuracy",
    hint: "mez5e.FlagsElvenAccuracyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "halflingLucky": {
    name: "mez5e.FlagsHalflingLucky",
    hint: "mez5e.FlagsHalflingLuckyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "initiativeAdv": {
    name: "mez5e.FlagsInitiativeAdv",
    hint: "mez5e.FlagsInitiativeAdvHint",
    section: "Feats",
    type: Boolean
  },
  "initiativeAlert": {
    name: "mez5e.FlagsAlert",
    hint: "mez5e.FlagsAlertHint",
    section: "Feats",
    type: Boolean
  },
  "jackOfAllTrades": {
    name: "mez5e.FlagsJOAT",
    hint: "mez5e.FlagsJOATHint",
    section: "Feats",
    type: Boolean
  },
  "observantFeat": {
    name: "mez5e.FlagsObservant",
    hint: "mez5e.FlagsObservantHint",
    skills: ['prc','inv'],
    section: "Feats",
    type: Boolean
  },
  "powerfulBuild": {
    name: "mez5e.FlagsPowerfulBuild",
    hint: "mez5e.FlagsPowerfulBuildHint",
    section: "Racial Traits",
    type: Boolean
  },
  "reliableTalent": {
    name: "mez5e.FlagsReliableTalent",
    hint: "mez5e.FlagsReliableTalentHint",
    section: "Feats",
    type: Boolean
  },
  "remarkableAthlete": {
    name: "mez5e.FlagsRemarkableAthlete",
    hint: "mez5e.FlagsRemarkableAthleteHint",
    abilities: ['str','dex','con'],
    section: "Feats",
    type: Boolean
  },
  "weaponCriticalThreshold": {
    name: "mez5e.FlagsWeaponCritThreshold",
    hint: "mez5e.FlagsWeaponCritThresholdHint",
    section: "Feats",
    type: Number,
    placeholder: 20
  },
  "spellCriticalThreshold": {
    name: "mez5e.FlagsSpellCritThreshold",
    hint: "mez5e.FlagsSpellCritThresholdHint",
    section: "Feats",
    type: Number,
    placeholder: 20
  },
  "meleeCriticalDamageDice": {
    name: "mez5e.FlagsMeleeCriticalDice",
    hint: "mez5e.FlagsMeleeCriticalDiceHint",
    section: "Feats",
    type: Number,
    placeholder: 0
  },
};
 
// Configure allowed status flags
mez5e.allowedActorFlags = ["isPolymorphed", "originalActor"].concat(Object.keys(mez5e.characterFlags));
 

